import React, { useState, useEffect } from "react";
import CardsContainer from "../components/CardsContainer.component";
import SearchBar from "../components/SearchBar.component";
import { GiphyFetch } from "@giphy/js-fetch-api";

const giphy = new GiphyFetch(process.env.REACT_APP_API_KEY);

const Home = () => {
    const [search, setSearch] = useState(""),
        [searchLabel, setSearchLabel] = useState(""),
        [results, setResults] = useState([]),
        [searchHistory, setSearchHistory] = useState(
            localStorage.getItem("searchHistory")
                ? JSON.parse(localStorage.getItem("searchHistory"))
                : localStorage.setItem("searchHistory", JSON.stringify([]))
        );

    const saveToHistory = (search) => {
        const searchHistoryData = JSON.parse(
            localStorage.getItem("searchHistory")
        );

        if (!searchHistoryData.includes(search) && search) {
            searchHistoryData.push(search);
            if (searchHistoryData.length > 5) {
                searchHistoryData.shift();
            }
        }

        localStorage.setItem(
            "searchHistory",
            JSON.stringify(searchHistoryData)
        );
        setSearchHistory(searchHistoryData);
    };

    const deleteHistoryItem = (deleteID) => {
        const searchHistoryData = JSON.parse(
            localStorage.getItem("searchHistory")
        );
        searchHistoryData.splice(searchHistoryData.indexOf(deleteID), 1);

        localStorage.setItem(
            "searchHistory",
            JSON.stringify(searchHistoryData)
        );
        setSearchHistory(searchHistoryData);
    };

    const handleInput = (e) => {
        setSearchLabel(e.target.value);
    };

    const onHistoryClick = (label, event) => {
        if (event.target.localName === "div") {
            setSearchLabel(label);
            setSearch(label);
        }
    };

    const getDogGifs = async () => {
        const { data: gifs } = await giphy.search(`dogs ${searchLabel}`, {
            sort: "relevant",
            lang: "es",
            limit: 12,
            type: "stickers",
        });
        saveToHistory(searchLabel);
        setResults(gifs);
    };

    useEffect(() => {
        getDogGifs();
        // eslint-disable-next-line
    }, [search]);
    
    useEffect(() => {}, [searchHistory]);

    return (
        <>
            <SearchBar
                value={searchLabel}
                onChange={handleInput}
                onSearch={getDogGifs}
                onHistoryDelete={deleteHistoryItem}
                onHistoryClick={onHistoryClick}
                searchHistory={searchHistory}
            />
            {results && <CardsContainer gifs={results} />}
        </>
    );
};

export default Home;
