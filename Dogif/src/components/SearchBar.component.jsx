import React from "react";
import History from "./History.component";

const SearchBar = ({
    value,
    onChange,
    onSearch,
    onHistoryDelete,
    searchHistory,
    onHistoryClick,
}) => {
    const handleKeyDown = (event) => {
        if (event.key === "Enter") onSearch();
    };

    return (
        <div className="max-w-md m-auto pl-10 pr-10 relative">
            <div className="text-center flex justify-center items-center mb-3">
                <input
                    type="text"
                    className="p-4 font-poppins rounded-l-3xl w-full font-bold border-solid border-2 border-black min-w-60"
                    placeholder="SEARCH"
                    value={value}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                />
                <button
                    className="text-white bg-callToAction font-poppins p-4 rounded-r-3xl border-solid border-2 border-black border-l-0"
                    onClick={onSearch}
                >
                    Go
                </button>
            </div>
            <History
                searchHistory={searchHistory}
                onHistoryDelete={onHistoryDelete}
                onHistoryClick={onHistoryClick}
            />
        </div>
    );
};

export default SearchBar;
