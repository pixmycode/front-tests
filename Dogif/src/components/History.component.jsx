import React from "react";
import { IoCloseCircle } from "react-icons/io5";

const History = ({ searchHistory, onHistoryDelete, onHistoryClick }) => {
    return (
        <div className={`b-0 z-10 p-4 pl-0`}>
            {searchHistory.map((historyData) => (
                <div
                    onClick={(event) => onHistoryClick(historyData, event)}
                    className="bg-white p-2 pl-3 rounded-full border-solid border-2 border-black inline-block mr-2 mb-2 hover:bg-callToAction transition-all group hover:cursor-pointer hover:text-white"
                    key={historyData}
                >
                    {historyData}
                    <button
                        className="text-2xl ml-1 text-callToAction align-middle group-hover:text-white transition-all"
                        onClick={() => onHistoryDelete(historyData)}
                        data-history-id={historyData}
                    >
                        <IoCloseCircle className="pointer-events-none" />
                    </button>
                </div>
            ))}
        </div>
    );
};

export default History;
