import { useState } from "react";
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { MdOutlineFavorite } from "react-icons/md";

const Card = ({ url, id }) => {
    const [favoriteGifIds, setFavoriteGifIds] = useState(
        localStorage.getItem("favoriteGifIds")
            ? JSON.parse(localStorage.getItem("favoriteGifIds"))
            : localStorage.setItem("favoriteGifIds", JSON.stringify([]))
    );

    const saveToFavorites = () => {
        const tempFavoriteGifIds = JSON.parse(
                localStorage.getItem("favoriteGifIds")
            )

        if (id && !tempFavoriteGifIds.includes(id)) {
            tempFavoriteGifIds.push(id);
        } else {
            tempFavoriteGifIds.splice(tempFavoriteGifIds.indexOf(id), 1);
        }

        const jsonFavGifIds = JSON.stringify(tempFavoriteGifIds);
        localStorage.setItem("favoriteGifIds", jsonFavGifIds);
        setFavoriteGifIds(jsonFavGifIds);
    };

    return (
        <div className="card">
            <img
                alt="gif"
                src={
                    url
                        ? url
                        : `https://media3.giphy.com/media/${id}/giphy-preview.gif`
                }
            />
            <button
                onClick={() => saveToFavorites()}
                className="absolute bottom-3 right-3 bg-callToAction text-white p-3 rounded-full"
            >
                {favoriteGifIds && favoriteGifIds.includes(id) ? (
                    <MdOutlineFavorite className="pointer-events-none" />
                ) : (
                    <MdOutlineFavoriteBorder className="pointer-events-none" />
                )}
            </button>
        </div>
    );
};

export default Card;
